import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HeaderComponent } from "./component/Header/header.component"
import { FirstComponent } from "./component/First/first.component";
import { SecondComponent } from "./component/Second/second.component";
import { ThirdComponent } from "./component/Third/third.component"
import { FourthComponent } from "./component/Fourth/fourth.component";
import { FifthComponent } from "./component/Fifth/fifth.component";
import { SixthComponent } from "./component/Sixth/sixth.component";
import { SeventhComponent } from "./component/Seventh/seventh.component"
import { FooterComponent } from "./component/Footer/footer.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FirstComponent,
    SecondComponent,
    ThirdComponent,
    FourthComponent,
    FifthComponent,
    SixthComponent,
    SeventhComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
