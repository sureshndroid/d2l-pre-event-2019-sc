import { Component, DoCheck } from "@angular/core"

@Component({
    selector: "m-fifth",
    templateUrl: "./fifth.component.html",
    styleUrls: ["./fifth.component.css"]
})

export class FifthComponent implements DoCheck {

    event: number = 1;

    ngDoCheck() {
        console.log(this.event, "XXX");
    }

    
    onItemClick(identifier) {
        switch (identifier) {
            case "india":
                this.event = 1;

                return true;
            case "singapore":
                this.event = 2;

                return true;

            case "asia":
                this.event = 3;

                return true;
        }
    }
}